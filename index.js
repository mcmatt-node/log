"use strict";

let clc = require('cli-color');
let now = require('now');

let cText = clc.whiteBright;
let cInfo = clc.cyanBright;
let cSuccess = clc.greenBright;
let cWarn = clc.yellowBright;
let cError = clc.redBright;

let time = () => cText(`[${now()}]`);

class Log {
    static text(text, colour) {
        console.log((colour || cText)(`${time()} ${text}`));
    }

    static info(text) {
        Log.text(text, cInfo);
    }

    static success(text) {
        Log.text(text, cSuccess);
    }

    static warn(text) {
        Log.text(text, cWarn);
    }

    static error(text) {
        Log.text(text, cError);
    }
};

module.exports = Log;
